package com.employee.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.employee.models.EmployeeLeave;
import com.employee.repositories.EmployeeLeaveRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployeeLeaveController {
	
	@Autowired
	private EmployeeLeaveRepo employeeLeaveRepo;

	@GetMapping("/employeeleave")
	public ResponseEntity<List<EmployeeLeave>> GetAllEmployeeLeave() {
		try {
			List<EmployeeLeave> employee = this.employeeLeaveRepo.findAll();
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("/employeeleave")
	public ResponseEntity<Object> SaveEmployeeLeave(@RequestBody EmployeeLeave employeeLeave) {
		try {
			employeeLeave.setCreatedBy("Lyauss");
			employeeLeave.setCreatedAt(new Date());
			this.employeeLeaveRepo.save(employeeLeave);
			return new ResponseEntity<>(employeeLeave, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/employeeleavemapped")
	public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<EmployeeLeave> employeeLeave = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<EmployeeLeave> pageTuts;

			pageTuts = employeeLeaveRepo.findAll(pagingSort);

			employeeLeave = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("employeeLeave", employeeLeave);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employeeleave/{id}")
	public ResponseEntity<List<EmployeeLeave>> GetEmployeeLeaveById(@PathVariable("id") Long id) {
		try {
			Optional<EmployeeLeave> employeeLeave = this.employeeLeaveRepo.findById(id);
			if (employeeLeave.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(employeeLeave, HttpStatus.OK);
				return rest;
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception exception) {
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		}
	}
	
	//
	@PutMapping("/employeeleave/{id}")
	public ResponseEntity<Object> UpdateEmployeeLeave(@RequestBody EmployeeLeave employeeLeave, @PathVariable("id") Long id) 
	{
		Optional<EmployeeLeave> employeeLeaveData = this.employeeLeaveRepo.findById(id);
		if (employeeLeaveData.isPresent()) {
			employeeLeave.setModifiedBy("Lyauss");
			employeeLeave.setModifiedAt(new Date());
			employeeLeave.setId(id);
			this.employeeLeaveRepo.save(employeeLeave);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@DeleteMapping("/employeeleave/{id}")
	public ResponseEntity<Object> DeleteEmployeeLeave(@PathVariable("id") Long id) 
	{
		Optional<EmployeeLeave> employeeLeaveData = this.employeeLeaveRepo.findById(id);
		if (employeeLeaveData.isPresent()) {
			this.employeeLeaveRepo.deleteById(id);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@GetMapping("/searchemployeeleave/{keyword}")
    public ResponseEntity<List<EmployeeLeave>> SearchCategoryName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<EmployeeLeave> employeeLeave = this.employeeLeaveRepo.SearchEmployeeLeave(keyword);
            return new ResponseEntity<>(employeeLeave, HttpStatus.OK);
        } else {
            List<EmployeeLeave> employeeLeave = this.employeeLeaveRepo.findAll();
            return new ResponseEntity<>(employeeLeave, HttpStatus.OK);
        }
}}
