package com.employee.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.employee.models.Biodata;
import com.employee.repositories.BiodataRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {
	
	@Autowired
	private BiodataRepo biodataRepo;
	
	@GetMapping("/biodata")
	public ResponseEntity<List<Biodata>> GetAllBiodata() {
		try {
			List<Biodata> biodata = this.biodataRepo.findAll();
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("/biodatamapped")
	public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Biodata> biodata = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<Biodata> pageTuts;

			pageTuts = biodataRepo.findAll(pagingSort);

			biodata = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("biodata", biodata);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/biodata/{id}")
	public ResponseEntity<List<Biodata>> GetBiodataById(@PathVariable("id") Long id) {
		try {
			Optional<Biodata> biodata = this.biodataRepo.findById(id);
			if (biodata.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
				return rest;
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception exception) {
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		}
	}
}
