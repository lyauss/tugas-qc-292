package com.employee.models;

import jakarta.persistence.*;

@Entity
@Table(name = "employee")
//@SQLDelete(sql = "UPDATE category SET is_delete = true WHERE id=?")
//@Where(clause = "is_delete=false")
public class Employee extends CommonEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private Long id;
	
	@Column(name = "biodata_id", nullable = false)
	private Long BiodataId;
	
	@ManyToOne
	@Column(name = "biodata_id", insertable = false, updatable = false)
	private Long Biodata;
	
	@Column(name = "nip")
	private String Nip;
	
	@Column(name = "status")
	private String Status;
	
	@Column(name = "salary")
	private Integer Salary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return BiodataId;
	}

	public void setBiodataId(Long biodataId) {
		BiodataId = biodataId;
	}

	public Long getBiodata() {
		return Biodata;
	}

	public void setBiodata(Long biodata) {
		Biodata = biodata;
	}

	public String getNip() {
		return Nip;
	}

	public void setNip(String nip) {
		Nip = nip;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Integer getSalary() {
		return Salary;
	}

	public void setSalary(Integer salary) {
		Salary = salary;
	}
	
	
	

	
}
