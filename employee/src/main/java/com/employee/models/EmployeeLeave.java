package com.employee.models;

import jakarta.persistence.*;

@Entity
@Table(name = "employeeLeave")
public class EmployeeLeave extends CommonEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private long id;
	
	@Column(name = "employee_id", nullable = false)
	private Long EmployeeId;
		
	@Column(name = "period", nullable = false)
	private Integer Period;
		
	@Column(name = "regular_quota", nullable = false)
	private Integer RegulerQuota;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(Long employeeId) {
		EmployeeId = employeeId;
	}

	public Integer getPeriod() {
		return Period;
	}

	public void setPeriod(Integer period) {
		Period = period;
	}

	public Integer getRegulerQuota() {
		return RegulerQuota;
	}

	public void setRegulerQuota(Integer regulerQuota) {
		RegulerQuota = regulerQuota;
	}
	
	
	
}
