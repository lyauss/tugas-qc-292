package com.employee.models;

import jakarta.persistence.*;

@Entity
@Table(name = "biodata")
public class Biodata extends CommonEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id",nullable = false)
	private Long id;
	
	@Column(name = "first_name", nullable = false)
	private Long FirstName;
	
	@Column(name = "last_name", nullable = false)
	private Long LastName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFirstName() {
		return FirstName;
	}

	public void setFirstName(Long firstName) {
		FirstName = firstName;
	}

	public Long getLastName() {
		return LastName;
	}

	public void setLastName(Long lastName) {
		LastName = lastName;
	}
	
	
	
}
