package com.employee.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.models.Biodata;

public interface BiodataRepo extends JpaRepository<Biodata, Long>{
	List<Biodata> SearchBiodata(String keyword);
}
