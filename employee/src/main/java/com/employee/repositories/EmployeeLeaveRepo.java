package com.employee.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.models.EmployeeLeave;

public interface EmployeeLeaveRepo extends JpaRepository<EmployeeLeave, Long>{
	List<EmployeeLeave> SearchEmployeeLeave(String keyword);
}
