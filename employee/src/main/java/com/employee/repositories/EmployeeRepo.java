package com.employee.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.employee.models.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long>{
	@Query("FROM Employee WHERE lower(EmployeeName) LIKE lower(concat('%',?1,'%') ) ")
    List<Employee> SearchEmployee(String keyword);
}
